<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- <%@ page language="java" contentType="text/html; charset=ISO-8859-1" --%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!-- pageEncoding="ISO-8859-1"%> -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="<c:url value="/resources/css/dashboard_style.css" />"
	rel="stylesheet"
>

</head>
<body>
	<div class="dashboard">
		<div class="pbchat header">
			<div class="pbchat-4 ">
				<ul class="pagelinks floatright">
					<li><a href="${pageContext.request.contextPath}/logout">Logout</a></li>
				</ul>
			</div>
		</div>
		<div class="pbchat ">
			<div class="pbchat-12 txtCenter ">
				<h3 class="txtupper bluecolor">You have been successfully
					logged out</h3>
			</div>
			<div align="center">
				<a href="${pageContext.request.contextPath}/">Login to Admin
					Panel Page</a>
			</div>

			<div data-role="footer" style="height: 2em;" data-theme="f"
				data-position="fixed"
			>
				<h1></h1>
			</div>
		</div>
		</div>
</body>
</html>