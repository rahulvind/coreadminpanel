<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- <%@ page language="java" contentType="text/html; charset=ISO-8859-1" --%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!-- pageEncoding="ISO-8859-1"%> -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- <link rel="stylesheet"
	href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css"> -->
<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<link href="<c:url value="/resources/css/dashboard_style.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/dashboard_style.css" />" rel="stylesheet">
<script type="text/javascript" src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="//cdn.jsdelivr.net/webshim/1.14.5/polyfiller.js"></script>

<style>
.ui-bar-f {
	color: SteelBlue;
	background-color: SteelBlue;
}

.ui-body-f {
	font-weight: bold;
	color: white;
	background-color: black;
}

.ui-page-theme-f {
	font-weight: bold;
	background-color: seaShell;
}

.t1 {
	color: grey;
	font-size: 24pt;
	font-weight: normal;
}

.t2 {
	color: black;
	font-size: 18pt;
	font-weight: normal;
}
</style>

<script type="text/javascript">
	function WriteCookie() {
		var now = new Date;
		var utc_timestamp = Date.UTC(now.getUTCFullYear(), now.getUTCMonth(),
				now.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(), now
						.getUTCSeconds(), now.getUTCMilliseconds());
		var date = new Date(utc_timestamp);
		cookievalue = date;
		/* new Date().getTimezoneOffset();  */
		document.cookie = "last_access_t=" + cookievalue;
	}
</script>

</head>
<body onload="myFunction()">

	<div class="dashboard">
		<div class="pbchat header">

			<div class="pbchat-4 ">
				<ul class="pagelinks floatright">
					<li><a href="${pageContext.request.contextPath}/logout">Logout</a></li>
				</ul>
			</div>
		</div>
		<div align="center">
			<br> <br> <br> <br>
			<div style="color:green">Please Login</div>
			<form action="${pageContext.request.contextPath}/login" method="post" id="user_form">
				<table>
					<tr>
						<td>Username:</td>
						<td><input type='text' name='username' placeholder="Enter username here" /></td>
					</tr>
					<tr>
						<td>Password:</td>
						<td><input type='password' name='password' placeholder="Enter password here"></td>
					</tr>
				</table>
				<button type="submit" data-inline="true" onclick="WriteCookie();">Login</button>

			</form>
		</div>
	</div>
</body>
</html>
