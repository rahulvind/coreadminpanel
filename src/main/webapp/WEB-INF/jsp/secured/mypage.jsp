<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!-- pageEncoding="ISO-8859-1"%> -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="<c:url value="/resources/css/dashboard_style.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/dashboard_style.css" />" rel="stylesheet">
<script type="text/javascript" src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
		<script src="//cdn.jsdelivr.net/webshim/1.14.5/polyfiller.js"></script>



<script type="text/javascript">
		$(document)
				.ready(
						function() {

							$("#uploadForm")
									.submit(
											function(event) {
												if ($("#file")[0].value === '') {
													event.preventDefault();
													alert('Please choose a file');
												} else {
													var fileSize = $("#file")[0].files[0].fileSize;

													if (fileSize === undefined
															|| fileSize === '') {
														fileSize = $("#file")[0].files[0].size;
													}if (fileSize === undefined
															|| fileSize === ''
															|| fileSize === 0) {
														event.preventDefault();
														alert('Please choose a file');
													} else {
														$("#uploadForm")
																.submit();
													}
												}

											});
						});
	</script>

</head>
<body>
	<div class="dashboard">
		<div class="pbchat header">
			<div class="pbchat-4 ">
				<ul class="pagelinks floatright">
					<li><a href="${pageContext.request.contextPath}/logout">Logout</a></li>
				</ul>
				<ul class="pagelinks floatleft">
					<sec:authentication var="principal" property="principal" />
					<li><a>Hi ${principal.username}</a></li>
				</ul>
			</div>
		</div>
		<div class="pbchat ">
			<div class="pbchat-12 txtCenter ">
				<h3 class="txtupper bluecolor">Services in Admin Panel</h3>
			</div>

		</div>
		<div align="center">

			<form method="POST" action="/ap/readFile" id="uploadForm" enctype="multipart/form-data">
				<input type="file" name="file" id="file">
				<!-- <input type="submit" name='submit' value='submit'> -->
				<button class="btn margin0">submit</button>
			</form>



			<c:if test="${flag==1}">
				<table style='table-layout: fixed; width: 20%'>
					<thead>
						<tr>
							<th>Param</th>
							<th>Count</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Line</td>
							<td>${line}</td>
						</tr>
						<tr>
							<td>Word</td>
							<td>${word}</td>
						</tr>
						<tr>
							<td>Characters</td>
							<td>${charac}</td>
						</tr>
					</tbody>
				</table>

			</c:if>

		</div>
		<div data-role="footer" style="height: 2em;" data-theme="f" data-position="fixed">
			<h1></h1>
		</div>
	</div>
</body>
</html>