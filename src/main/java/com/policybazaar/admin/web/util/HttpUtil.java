package com.policybazaar.admin.web.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This utility class is used to call interact with various HTTP supported methods to call from different level in the project.
 * 
 */
/**
 * @author Rahulv
 *
 */
public class HttpUtil {

	private static final Logger logger = LoggerFactory.getLogger(HttpUtil.class);

	/**
	 * This method is used to get the HTML content by provided URL with GET method.
	 * 
	 * @param targetURL
	 * @return
	 * @throws IOException
	 * @throws ClientProtocolException
	 */
	public static String getContentByURL(String targetURL) throws ClientProtocolException, IOException {
		String content = null;
		HttpClient client = HttpClientBuilder.create().build();
		HttpRequestBase request;
		request = new HttpGet(targetURL);
		// List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		// request.setHeader("User-Agent", "JAVA_CLIENT");
		// request.setHeader("Content-Type", "application/json");
		try {
			HttpResponse response = client.execute(request);
			String responseAsString = EntityUtils.toString(response.getEntity());
			content = responseAsString;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		} finally {
			client = null;
		}

		return content;
	}

	/**
	 * This method is used to get Content by URL with
	 * 
	 * @param targetURL
	 * @param nameValPairs
	 * @return
	 */
	public static String getContentByURL(String targetURL, Map<String, String> nameValPairs) {
		String content = null;
		HttpClient client = null ;
		try {
			client = HttpClientBuilder.create().build();
			HttpRequestBase request;

			List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
			Iterator<Entry<String, String>> iterator = nameValPairs.entrySet().iterator();
			while (iterator.hasNext()) {
				Entry<String, String> entry = iterator.next();
				urlParameters.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
				logger.debug("Key : " + entry.getKey() + " and Value: " + entry.getValue());
				// System.out.printf("Key : %s and Value: %s %n",
				// entry.getKey(), entry.getValue());
				iterator.remove(); // right way to remove entries from Map,
									// avoids ConcurrentModificationException
			}

			// request.setHeader("User-Agent", "JAVA_CLIENT");
			// request.setHeader("Content-Type", "application/json");
			String query = URLEncodedUtils.format(urlParameters, "utf-8");
			// request.setEntity(new UrlEncodedFormEntity(urlParameters));
			targetURL += query;
			request = new HttpGet(targetURL);
			HttpResponse response = client.execute(request);
			if (response != null) {
				content = EntityUtils.toString(response.getEntity());
			}
		} catch (ClientProtocolException e) {
			logger.error("ClientProtocolException caught wile geting content on URI=" + targetURL + " , msg:" + e.getMessage());
		} catch (IOException e) {
			logger.error("IOException caught wile geting content on URI=" + targetURL + " , msg:" + e.getMessage());
		} finally {
			client = null ;
		}
		return content;
	}

	public static Map<String, Object> post(String uri, String jsonPayload) throws Exception {
		HttpResponse httpResponse = null;
		Map<String, Object> responseAttributes = new HashMap<String, Object>();
		HttpClient httpclient = null ;
		try {
			// instantiates httpclient to make request
			httpclient = HttpClientBuilder.create().build();

			// url with the post data
			HttpPost httpost = new HttpPost(uri);

			// convert parameters into JSON object
			// JSONObject holder = getJsonObjectFromMap(headerparams);

			// passes the results to a string builder/entity
			StringEntity se = new StringEntity(jsonPayload);

			// sets the post request as the resulting string
			httpost.setEntity(se);

			// sets a request header so the page receving the request will know
			// what to do with it
			httpost.setHeader("Accept", "application/json");
			httpost.setHeader("Content-type", "application/json");

			// Handles what is returned from the page
			// /ResponseHandler responseHandler = new BasicResponseHandler();
			httpResponse = httpclient.execute(httpost);
			String responseAsString = "";
			int statusCode = 0;
			if (httpResponse != null) {
				responseAsString = EntityUtils.toString(httpResponse.getEntity());
				statusCode = httpResponse.getStatusLine().getStatusCode();
				logger.debug(responseAsString);
			} else {
				logger.debug("No response receceived from[ " + uri + " ], for payload[ " + jsonPayload + " ]");
			}

			responseAttributes.put("responseBody", responseAsString);
			responseAttributes.put("status", statusCode);
		} catch (ClientProtocolException e) {
			logger.error("ClientProtocolException caught wile posting json payload on URI=" + uri + " , msg:" + e.getMessage());
		} catch (Exception e) {
			logger.error("Exception caught wile posting json payload on URI=" + uri + " , msg:" + e.getMessage());
		} finally {
			httpclient = null ;
		}
		return responseAttributes;
	}

	/**
	 * This method is used to post on uri with JSON payLoad.
	 * 
	 * @param uri
	 * @param jsonPayload
	 * @return
	 * @throws Exception
	 */
	public static int postRequestWithJsonPayload(String uri, String jsonPayload) throws Exception {
		int statusCode = 0;
		Map<String, Object> response = post(uri, jsonPayload);
		if (response != null) {
			statusCode = response.get("status") != null ? Integer.parseInt(response.get("status").toString()) : statusCode;
			logger.debug(response.toString());
		}
		return statusCode;
	}

	/**
	 * @param uri
	 * @param jsonPayload
	 * @param header
	 * @return
	 * @throws Exception
	 */
	public static Map<String, Object> post(String uri, String jsonPayload, Map<String, String> header) throws Exception {
		HttpResponse httpResponse = null;
		Map<String, Object> responseAttributes = new HashMap<String, Object>();
		HttpClient httpclient  = null ;
		try {
			// instantiates httpclient to make request
			httpclient = HttpClientBuilder.create().build();

			// url with the post data
			HttpPost httpost = new HttpPost(uri);

			// passes the results to a string builder/entity
			StringEntity se = new StringEntity(jsonPayload);

			// sets the post request as the resulting string
			httpost.setEntity(se);

			// sets a request header so the page receving the request will know
			// what to do with it
			if (header != null) {
				for (Map.Entry<String, String> entry : header.entrySet()) {
					httpost.setHeader(entry.getKey(), entry.getValue());
				}
			}
			httpost.setHeader("Accept", "application/json");
			httpost.setHeader("Content-type", "application/json");

			// Handles what is returned from the page
			// /ResponseHandler responseHandler = new BasicResponseHandler();
			httpResponse = httpclient.execute(httpost);
			String responseAsString = "";
			int statusCode = 0;
			if (httpResponse != null) {
				responseAsString = EntityUtils.toString(httpResponse.getEntity());
				statusCode = httpResponse.getStatusLine().getStatusCode();
				logger.debug(responseAsString);
			} else {
				logger.debug("No response receceived from[ " + uri + " ], for payload[ " + jsonPayload + " ]");
			}

			responseAttributes.put("responseBody", responseAsString);
			responseAttributes.put("status", statusCode);
		} catch (ClientProtocolException e) {
			logger.error("ClientProtocolException caught wile posting json payload on URI=" + uri + " , msg:" + e.getMessage());
		} catch (Exception e) {
			logger.error("Exception caught wile posting json payload on URI=" + uri + " , msg:" + e.getMessage());
		} finally {
			httpclient = null ;
		}
		return responseAttributes;
	}
}
