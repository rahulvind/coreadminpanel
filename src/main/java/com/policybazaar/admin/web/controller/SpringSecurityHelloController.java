package com.policybazaar.admin.web.controller;

import java.io.File;
import java.io.IOException;
import java.security.Principal;
import java.util.Scanner;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author rahulv
 *
 */
@Controller
public class SpringSecurityHelloController {
	@RequestMapping("/secured/mypage")
	public String mypage(Model model, Principal principal) {
		String userName = principal.getName();
		model.addAttribute("message", userName);

		return "secured/mypage";
	}

	@RequestMapping(value = "/logoutPage", method = RequestMethod.GET)
	public String logoutPage() {
		return "logoutPage";
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String login() {
		return "loginPage";
	}

	@RequestMapping(value = "/readFile", method = RequestMethod.POST)
	public String uploadDocumentStore(@RequestParam("file") MultipartFile file, Model model) throws IOException {
		Scanner in = new Scanner(multipartToFile(file));
		int lineNum = 0;
		int wordCount = 1;
		int charCount = 0;

		while (in.hasNextLine()) {
			String line;
			line = in.nextLine();

			// output.println(lineNum + ": " + line);

			lineNum++;

			String str[] = line.split((" "));
			for (int i = 0; i < str.length; i++) {
				if (str[i].length() > 0) {
					wordCount++;
				}
			}
			charCount += (line.length());

		}
		in.close();
		model.addAttribute("flag", 1);
		model.addAttribute("line", lineNum);
		model.addAttribute("word", wordCount);
		model.addAttribute("charac", charCount);

		return "secured/mypage";
	}

	public File multipartToFile(MultipartFile multipart) throws IllegalStateException, IOException {
		File convFile = new File(multipart.getOriginalFilename());
		multipart.transferTo(convFile);
		return convFile;
	}
}
