package com.policybazaar.admin.web.controller;

public class SortController {

	public static void main(String[] args){
	    int n=4;
	    int c=0;
	    for(int i = 1;i<=n;i++){
	    System.out.println(SortController.series(c));
	    c++;
	    }
	}
	
	public static int series(int n){
		
		if(n==0){
			return 0;
		}else if(n==1){
			return 1;
		}else {
			return series(n-2)+series(n-1);
		}
		
	}
}
